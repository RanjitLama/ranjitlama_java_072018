package com.lama.oo.basic;

public class EmployeeTestClass {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Employee employee1 =new Employee();
		employee1.name = " Ranjit";
		employee1.ssn = " 852-52-6587";
		employee1.salary = 300000.00;
		
		Employee employee2 =new Employee();
		employee2.name = " John";
		employee2.ssn = " 932-14-1254";
		employee2.salary =  200000.00;
		
		System.out.println("Employee1 Info");
		employee1.printEmployeeInfo();
		employee1.printSocialSecurityAndName();

		System.out.println("Employee1 Info");
		employee2.printEmployeeInfo();
		employee2.printSocialSecurityAndName();
		
		Employee[] empArray =new Employee[2];
		empArray[0]= employee1;
		empArray[1]= employee2;
		
		Employee[] empArray2 = {employee1, employee2};
	}

}
