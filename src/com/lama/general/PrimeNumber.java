package com.lama.general;

public class PrimeNumber {

	public static void main(String[] args) {		
		int i = 2;
		int checkNumber = 13;
		boolean isPrime = true;
		
		do {
			if (checkNumber % i ==0)
				isPrime = false;
			i++;
		}while(i<checkNumber);
			
		if(isPrime) {
			System.out.println(checkNumber + " is Prime Number");
		}
		else {
			System.out.println(checkNumber + " is Not a Prime Number");
		}

	}
}
