package com.lama.general;

public class LoopConcept {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		for(int i = 0; i < 5; i++) {
			System.out.println("My Name Is Ranjit Lama");
		}
			
		int j = 0;
		while(j < 5) {
			System.out.println("I'm in Java class");
			j++;
			System.out.println("Value of j = " + j);
		}
			
			
	}

}
